# Sofbound Books

Softbound Books will be a website where you can share, rent, or sell any book you have with the rest of the world to contribute to spare education and literate other people. Lots of people love reading a book live rather to online, and that the product that this website will offer as a service.

## License
https://opensource.org/licenses/ISC